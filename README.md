# robot-simu

[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/gvallat/robot-simu)

Goal: simulation a robot physique and experimenting with robot movement.

- use a game engine for visualization (bevy ?)
- use physic engine for gravity effect (rapier ?)
- experiment with robot movement (different robot shapes, walk, ...)
